#Python Files
sudo cp api_views.py /usr/lib/python2.7/dist-packages/kalite/updates
sudo cp api_urls.py /usr/lib/python2.7/dist-packages/kalite/updates 

#Html Files
sudo cp update_iqcontent.html /usr/lib/python2.7/dist-packages/kalite/updates/templates/updates  
sudo cp  update_videos.html /usr/lib/python2.7/dist-packages/kalite/updates/templates/updates  

#JS Files
sudo cp  bundle_update_iqcontent.js $HOME/.kalite/httpsrv/static/js/updates/bundles  
sudo cp  bundle_update_videos.js $HOME/.kalite/httpsrv/static/js/updates/bundles  

