#Python files
sudo cp api_urls.py /usr/lib/python2.7/dist-packages/kalite/updates
sudo cp api_views.py /usr/lib/python2.7/dist-packages/kalite/updates
sudo cp urls.py /usr/lib/python2.7/dist-packages/kalite/updates
sudo cp views.py /usr/lib/python2.7/dist-packages/kalite/updates
sudo cp iq_wifi_ap.py /usr/lib/python2.7/dist-packages/kalite/updates

#Html file
sudo cp update_iqsettings.html /usr/lib/python2.7/dist-packages/kalite/updates/templates/updates
sudo cp base_manage.html /usr/lib/python2.7/dist-packages/kalite/distributed/templates/distributed/

#JS file
sudo cp bunle_update_iqsettings.js $HOME/.kalite/httpsrv/static/js/updates/bundles
