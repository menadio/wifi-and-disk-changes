import os, re, json

user_home = os.path.expanduser("~")
config_file = user_home + '/Desktop/test_wifi.conf'

#UPDATES##########################################################################################################
def set_wifi_security(state='0', passwd=''):
    # Read contents from file as a single string    
    with open(config_file, 'r') as file_handle:
        file_string = file_handle.read()

    pattern = r'#{0,1}'

    auth_algs_subst = 'auth_algs='
    wpa_subst = 'wpa='
    wpa_passphrase_subst = 'wpa_passphrase='
    wpa_key_mgmt_subst = 'wpa_key_mgmt='
    wpa_pairwise_subst = 'wpa_pairwise='
    rsn_pairwise_subst = 'rsn_pairwise='

    auth_algs_line_pattern = re.compile(pattern + auth_algs_subst)
    wpa_line_pattern = re.compile(pattern + wpa_subst)
    wpa_passphrase_line_pattern = re.compile(pattern + wpa_passphrase_subst + r'.*\n')
    wpa_key_mgmt_line_pattern = re.compile(pattern + wpa_key_mgmt_subst)
    wpa_pairwise_line_pattern = re.compile(pattern + wpa_pairwise_subst)
    rsn_pairwise_line_pattern = re.compile(pattern + rsn_pairwise_subst)

    if state == '0':
        auth_algs_subst = '#'+auth_algs_subst
        wpa_subst = '#'+wpa_subst
        wpa_passphrase_subst = '#'+wpa_passphrase_subst
        wpa_key_mgmt_subst = '#'+wpa_key_mgmt_subst
        wpa_pairwise_subst = '#'+wpa_pairwise_subst
        rsn_pairwise_subst = '#'+rsn_pairwise_subst

    # Use RE package to allow for replacement (also allowing for (multiline) REGEX)
    file_string = (re.sub(auth_algs_line_pattern, auth_algs_subst, file_string))
    file_string = (re.sub(wpa_line_pattern, wpa_subst, file_string))
    file_string = (re.sub(wpa_passphrase_line_pattern, wpa_passphrase_subst+passwd+'\n', file_string))
    file_string = (re.sub(wpa_key_mgmt_line_pattern, wpa_key_mgmt_subst, file_string))
    file_string = (re.sub(wpa_pairwise_line_pattern, wpa_pairwise_subst, file_string))
    file_string = (re.sub(rsn_pairwise_line_pattern, rsn_pairwise_subst, file_string))

    # Write contents to file.
    with open(config_file, 'w') as file_handle:
        file_handle.write(file_string)


def set_ssid(subst):
    # Read contents from file as a single string    
    with open(config_file, 'r') as file_handle:
        file_string = file_handle.read()

    # Use RE package to allow for replacement (also allowing for (multiline) REGEX)
    ssid_line_pattern = re.compile(r'ssid.*\n')
    subst = 'ssid='+subst+'\n'
    file_string = (re.sub(ssid_line_pattern, subst, file_string))

    # Write contents to file.
    with open(config_file, 'w') as file_handle:
        file_handle.write(file_string)
#UPDATES##########################################################################################################


#CHECKS###########################################################################################################
def is_valid_ssid(ssid):
    """checks that ssid matches the naming convention:"""
    ssid_pattern = re.compile(u'^\S.{0,30}$') #all characters accepted but must not begin with whitespace, MAX 31
    if ssid_pattern.match(ssid):
        return True
    else:
        return False


def is_valid_passwd(passwd):
    """checks that passwd is valid"""
    passwd_pattern = re.compile(u'^.{8,63}$') #min/max password lenght (8-63)
    if passwd_pattern.match(passwd):
        return True
    else:
        return False
#CHECKS###########################################################################################################



def get_wifi_info():
    if os.path.isfile(config_file):
        wifiAP_installed='1'
        
        # Read contents from file as a single string    
        with open(config_file, 'r') as file_handle:
            file_string = file_handle.read()

        ssid = re.search(r'ssid=(.*)\n', file_string).group(1)
        passwd = re.search(r'wpa_passphrase=(.*)\n', file_string).group(1)

        security_enabled = re.search(r'^wpa_passphrase', file_string, re.MULTILINE)
        if security_enabled:
            passwd_enabled = '1'
        else:
            passwd_enabled = '0'

        result = {'ssid':ssid, 'passwd':passwd, 'passwd_enabled':passwd_enabled, 'wifi_installed':wifiAP_installed}

        return json.dumps(result)
    else:
        wifiAP_installed='0'
        
        result = {'wifi_installed':wifiAP_installed}
        
        return json.dumps(result)


#try:
#    set_ssid(config_file, 'ssid='+the_ssid+'\n')
#    set_wifi_security(config_file, 'on', 'jutssss')
#    raw_input("A restart is required to complete configuration \npress ENTER to end...")
#except:
#    print("something is wrong, must run as administrator, make sure the  WiFi access point has been setup\nor contact your system administrator for support.")


'''test for problematic characters in ssid and passwd:
ABCDEFGHIJKLMNOPQRSTUVWXYZ
1234567890
$@^`,|%;.~()/\{}:?[]=-+_#!
"<>'&
'''

#On load check if config file exists, if yes: load ssid and security state, if no disable wifi section and display message: wifiAP not configured
#front-end must enforce minimum/maximum characters for SSID (1-31) and Password (8-63)
#test special characters in SSID and Password fields

#ALGORITHM:
#1 when page loads
#if config_file exists:
#    return ssid, password, security_state
#else:
#    disable wifiAP controls on page and display wifiAp not configured/installed.
#2 get_ssid()
#3 get_password()
#4 get_security_state()

#when save page
#5 if security_state == enabled && is_valid_ssid && is_valid_passwd: save else, alert and change nothing.
#6 if security state == disabled && is_valid_ssid: save, else alert and change nothing.
